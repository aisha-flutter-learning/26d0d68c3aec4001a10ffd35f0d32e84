// import 'package:crop2x/views/errors/internet_error.dart';
// import 'package:crop2x/views/login/api_handler.dart';
// import 'package:forceupdate/forceupdate.dart';
// import 'package:new_version/new_version.dart';
// import 'package:crop2x/views/login/login.dart';
import 'package:flutter/material.dart';
// import "package:firebase_messaging/firebase_messaging.dart";
// import 'package:cloud_firestore/cloud_firestore.dart';
// import "package:firebase_core/firebase_core.dart";
import "package:font_awesome_flutter/font_awesome_flutter.dart";
// import 'package:flutter/services.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:connectivity_plus/connectivity_plus.dart';
// import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:url_launcher/url_launcher.dart';
// import 'package:webview_flutter/webview_flutter.dart';
// import 'dart:io';
// import "package:http/http.dart" as http;
// import "package:path_provider/path_provider.dart" as path_provider;
// import "package:workmanager/workmanager.dart";
// import 'package:permission_handler/permission_handler.dart';
// import 'package:timezone/data/latest_all.dart' as tz;
// import 'package:timezone/timezone.dart' as tz;
//adding comment to check the bitbucket and vscode link
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // Application name
      title: 'Flutter Hello World',
      // Application theme data, you can set the colors for the application as
      // you want
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // A widget which will be started on application startup
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final String title;

  const MyHomePage({required this.title});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // The title text which will be shown on the action bar
        title: Text(title),
      ),
      body: Column(children: [
        Center(
          child: Text(
            'Hello, World!',
          ),
        ),
        buildSocialButtons()
      ]),
    );
  }
}

enum SocialMedia { facebook, twitter, email, linkedin, whatsapp }

// Social Media Sharing Buttons
Widget buildSocialButton({@required icon, color, @required onClicked}) => InkWell(
    onTap: onClicked,
    child: Container(
      width: 40,
      height: 40,
      child: Center(
        child: FaIcon(icon, color: color, size: 20),
      ),
    ));

Widget buildSocialButtons() => Container(
      margin: EdgeInsets.only(top: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          buildSocialButton(
              icon: FontAwesomeIcons.facebookSquare,
              color: Color(0xFF1c6434),
              onClicked: () async {
                share(SocialMedia.facebook);
              }),
          buildSocialButton(icon: FontAwesomeIcons.twitterSquare, color: Color(0xFF1c6434), onClicked: () => share(SocialMedia.twitter)),
          buildSocialButton(icon: FontAwesomeIcons.linkedin, color: Color(0xFF1c6434), onClicked: () => share(SocialMedia.linkedin)),
          buildSocialButton(icon: FontAwesomeIcons.whatsapp, color: Color(0xFF1c6434), onClicked: () => share(SocialMedia.whatsapp)),
          buildSocialButton(icon: FontAwesomeIcons.google, color: Color(0xFF1c6434), onClicked: () => share(SocialMedia.email))
        ],
      ),
    );

// Opens up the selected platform for sharing
Future share(SocialMedia socialPlatform) async {
  final subject = "Crop2x Mobile App";
  final text = "Crop2x Mobile App";
  final urlShare = Uri.encodeComponent("https://play.google.com/store/apps/details?id=com.crop2x.crop2x");
  final urls = {
    SocialMedia.facebook: 'https://www.facebook.com/sharer/sharer.php?u=$urlShare&t=$text',
    SocialMedia.twitter: 'https://twitter.com/intent/tweet?url=$urlShare&text=$text',
    SocialMedia.whatsapp: "whatsapp://send?text=Crop2X Mobile App: $urlShare",
    SocialMedia.email: "mailto:?subject=$subject&body=$urlShare",
    SocialMedia.linkedin: "https://www.linkedin.com/shareArticle?mini=true&url=$urlShare"
  };

  final url = urls[socialPlatform];
  if (await canLaunch(url.toString())) {
    await launch(url.toString());
  }
}
